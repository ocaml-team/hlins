(***************************************************************************)
(*  HLins: insert http-links into HTML documents.                          *)
(*  See http://www.lri.fr/~treinen/hlins                                   *)
(*                                                                         *)
(*  Copyright (C) 1999-2024 Ralf Treinen <treinen@irif.fr>                 *)
(*                                                                         *)
(*  This program is free software; you can redistribute it and/or modify   *)
(*  it under the terms of the GNU General Public License as published by   *)
(*  the Free Software Foundation; either version 2 of the License, or (at  *)
(*  your option) any later version.                                        *)
(*                                                                         *)
(*  This program is distributed in the hope that it will be useful, but    *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU      *)
(*  General Public License for more details.                               *)
(*                                                                         *)
(*  You should have received a copy of the GNU General Public License      *)
(*  along with this program; if not, write to the Free Software            *)
(*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307    *)
(*  USA                                                                    *)
(*                                                                         *)
(***************************************************************************)

(*
    HLins: insert http-links into HTML documents.
    See http://www.lri.fr/~treinen/hlins

    Copyright (C) 1999 Ralf Treinen <treinen@lri.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

{
open Lexing;;
open Buffer;;

(* translates an html character representation "&#nnn;" to the 
   corresponding 8-bit char.
*)
let special_to_char s = Char.chr (int_of_string (String.sub s 2 3))
;;

type token =
    CHAR of char
  | VERB of string;;

let b = create 1024;;

}

let space = [' ''\n''\t''\r']


rule next_html = parse
  | "&Agrave;"	{ CHAR '�' }
  | "&Aacute;"	{ CHAR '�' }
  | "&Acirc;"	{ CHAR '�' }
  | "&Atilde;"	{ CHAR '�' }
  | "&Auml;"	{ CHAR '�' }
  | "&Aring;"	{ CHAR '�' }
  | "&AElig;"	{ CHAR '�' }
  | "&Egrave;"	{ CHAR '�' }
  | "&Eacute;"	{ CHAR '�' }
  | "&Ecirc;"	{ CHAR '�' }
  | "&Euml;"	{ CHAR '�' }
  | "&Igrave;"	{ CHAR '�' }
  | "&Iacute;"	{ CHAR '�' }
  | "&Icirc;"	{ CHAR '�' }
  | "&Iuml;"	{ CHAR '�' }
  | "&ETH;"	{ CHAR '�' }
  | "&Ntilde;"	{ CHAR '�' }
  | "&Ograve;"	{ CHAR '�' }
  | "&Oacute;"	{ CHAR '�' }
  | "&Ocirc;"	{ CHAR '�' }
  | "&Otilde;"	{ CHAR '�' }
  | "&Ouml;"	{ CHAR '�' }
  | "&Oslash;"	{ CHAR '�' }
  | "&Ugrave;"	{ CHAR '�' }
  | "&Uacute;"	{ CHAR '�' }
  | "&Ucirc;"	{ CHAR '�' }
  | "&Uuml;"	{ CHAR '�' }
  | "&Yacute;"	{ CHAR '�' }
  | "&THORN;"	{ CHAR '�' }
  | "&szlig;"	{ CHAR '�' }
  | "&agrave;"	{ CHAR '�' }
  | "&aacute;"	{ CHAR '�' }
  | "&acirc;"	{ CHAR '�' }
  | "&atilde;"	{ CHAR '�' }
  | "&auml;"	{ CHAR '�' }
  | "&aring;"	{ CHAR '�' }
  | "&aelig;"	{ CHAR '�' }
  | "&ccdil;"	{ CHAR '�' }
  | "&egrave;"	{ CHAR '�' }
  | "&eacute;"	{ CHAR '�' }
  | "&ecirc;"	{ CHAR '�' }
  | "&euml;"	{ CHAR '�' }
  | "&igrave;"	{ CHAR '�' }
  | "&iacute;"	{ CHAR '�' }
  | "&icirc;"	{ CHAR '�' }
  | "&iuml;"	{ CHAR '�' }
  | "&eth;"	{ CHAR '�' }
  | "&ntilde;"	{ CHAR '�' }
  | "&ograve;"	{ CHAR '�' }
  | "&oacute;"	{ CHAR '�' }
  | "&ocirc;"	{ CHAR '�' }
  | "&otilde;"	{ CHAR '�' }
  | "&ugrave;"	{ CHAR '�' }
  | "&uacute;"	{ CHAR '�' }
  | "&ucirc;"	{ CHAR '�' }
  | "&uuml;"	{ CHAR '�' }
  | "&yacute;"	{ CHAR '�' }
  | "&thorn;"	{ CHAR '�' }
      (* special characters between 192 and 255 *)
  | "&#" ( "19"['2'-'9'] | '2'['0'-'4']['0'-'9'] | "25"['0'-'5'] ) ";"
      { CHAR (special_to_char (lexeme lexbuf)) }
  | '<'('H'|'h')('E'|'e')('A'|'a')('D'|'d')'>'
      { add_string b (lexeme lexbuf); skip_head lexbuf }
  | '<'('C'|'c')('O'|'o')('D'|'d')('E'|'e')'>'
      { add_string b (lexeme lexbuf); skip_code lexbuf }
  | '<'('S'|'s')('A'|'a')('M'|'m')('P'|'p')'>'
      { add_string b (lexeme lexbuf); skip_samp lexbuf }
  | '<'('K'|'k')('B'|'b')('D'|'d')'>'
      { add_string b (lexeme lexbuf); skip_kbd lexbuf }
  | '<'('D'|'d')('I'|'i')('V'|'v') space+ "nohlins>"
      { add_string b (lexeme lexbuf); skip_div lexbuf }
  | '<'('A'|'a') space+ ([^'>']*space)?
      ('H'|'h')('R'|'r')('E'|'e')('F'|'f') '=' [^'>']* '>'
     { add_string b (lexeme lexbuf); skip_a lexbuf } 
  | '<' [^'>']* '>'   { VERB(lexeme lexbuf)}
  | eof               { raise End_of_file }
  | _                 { CHAR(lexeme_char lexbuf 0)}

and skip_head = parse
  |  "</"('H'|'h')('E'|'e')('A'|'a')('D'|'d')'>'
      { add_string b (lexeme lexbuf);
	let c = contents b in reset b; VERB(c)}
  | _ { add_char b (lexeme_char lexbuf 0); skip_head lexbuf }

and skip_code = parse
  |  "</"('C'|'c')('O'|'o')('D'|'d')('E'|'e')'>'
      { add_string b (lexeme lexbuf);
	let c = contents b in reset b; VERB(c) }
  | _ { add_char b (lexeme_char lexbuf 0); skip_code lexbuf }

and skip_samp = parse
  |  "</"('S'|'s')('A'|'a')('M'|'m')('P'|'p')'>'
      { add_string b (lexeme lexbuf);
	let c = contents b in reset b; VERB(c) }
  | _ { add_char b (lexeme_char lexbuf 0); skip_samp lexbuf }

and skip_kbd = parse
  |  "</"('K'|'k')('B'|'b')('D'|'d')'>'
      { add_string b (lexeme lexbuf);
	let c = contents b in reset b; VERB(c) }
  | _ { add_char b (lexeme_char lexbuf 0); skip_kbd lexbuf }

and skip_pre = parse
  |  "</"('P'|'p')('R'|'r')('E'|'e')'>'
      { add_string b (lexeme lexbuf);
	let c = contents b in reset b; VERB(c) }
  | _ { add_char b (lexeme_char lexbuf 0); skip_pre lexbuf }

and skip_div = parse
  |  "</"('D'|'d')('I'|'i')('V'|'v')'>'
      { add_string b (lexeme lexbuf);
	let c = contents b in reset b; VERB(c) }
  | _ { add_char b (lexeme_char lexbuf 0); skip_div lexbuf }

and skip_a = parse
  |  "</"('A'|'a')'>'
      { add_string b (lexeme lexbuf);
	let c = contents b in reset b; VERB(c) }
  | _ { add_char b (lexeme_char lexbuf 0); skip_a lexbuf }




