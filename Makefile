include Version
PROGVER = hlins-$(VERSION)


dummy: all

configure:
	(cd source; ./configure)

# if configured with "make configure":
all:
	(cd source; make)

install:
	(cd source; make install)
	(cd doc; make install)

# for ocaml bytecode compilation
all.bc:
	(cd source; make hlins.bc)

install.bc:
	(cd source; make install.bc)
	(cd doc; make install)


distrib:
	(cd doc/examples/hlins-documentation; make)
	make tarball

# cleanup and building tarball
tarball: distclean
	mkdir $(PROGVER)
	cp -r source doc Makefile Version $(PROGVER)
	-rm $(PROGVER)/*~ $(PROGVER)/*/*~ $(PROGVER)/*/*/*~
	tar --exclude="CVS" --exclude=".cvsignore"\
		-cvf $(PROGVER).tar $(PROGVER)
	gzip -f $(PROGVER).tar
	rm -r $(PROGVER)

clean:
	(cd source; make clean)
	(cd doc; make clean)

distclean:
	(cd source; make distclean)
	(cd doc; make distclean)
	-rm -r $(PROGVER)
