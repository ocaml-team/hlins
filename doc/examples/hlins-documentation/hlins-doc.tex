%     HLins: insert http-links into LaTeX documents.
%     See https://hlins.alioth.debian.org
%
%     Copyright (C) 1999-2003 Ralf Treinen <treinen@debian.org>
%
%     This program is free software; you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation; either version 2 of the License, or
%     (at your option) any later version.
%
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with this program; if not, write to the Free Software
%     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
\documentclass{article}
\usepackage[T1]{fontenc}
\usepackage{french}
\usepackage{alltt}
\usepackage{hevea}
\title{Hlins: Hyper-Link Insertions in HTML documents\\Version 0.39}
\author{Ralf Treinen}
\date{May 1, 2003}

\def\knuthurl{http://www-cs-staff.stanford.edu/...}
\def\lamporturl{http://www.research.digital.com/...}

\begin{document}

\maketitle

\section{An Introductory Example}
\emph{Hlins} inserts in a \url{http://www.w3.org/TR/html40}{HTML}
document the url's (uniform resource locator) for certain names
(normally the names of people), according to a data base associating
url's to names.

First you have to create a data base that associates url's to names,
let's call it \texttt{addresses}:
\begin{quote}
\small
\begin{alltt}
Donald Knuth        =  \knuthurl
Leslie Lamport      =  \lamporturl
\end{alltt}
\end{quote}

Suppose that you have a HTML document \texttt{mytext.html} that
contains text as
\begin{quote}
\small
\begin{alltt}
A milestone in the development of digital typesetting was the TeX
system developed by Stanford computer science professor Donald
Knuth, which was used by L. Lamport as a base to build the more user-friendly
(but less powerful) LaTeX system.
\end{alltt}
\end{quote}

Calling \texttt{hlins -db addresses -o newtext.html mytext.html} will
generate a file \texttt{newtext.html} that contains now the piece of
text
\begin{quote}
\small
\begin{verbatim}
A milestone in the development of digital typesetting was the TeX
system developed by Stanford computer science professor <a
href="http://www-cs-staff.stanford.edu/...">Donald Knuth</a>, which
was used by <a
href="http://www.research.digital.com/...">L. Lamport</a> as a base to
build the more user-friendly (but less powerful) LaTeX system.
\end{verbatim}
\end{quote}

which will eventually be rendered by a browser as something like

\begin{quote}
\small A milestone in the development of digital typesetting was the
TeX system developed by Stanford computer science professor
\url{http://www-cs-staff.stanford.edu/\%7eknuth/index.html}{Donald
Knuth}, which was used by
\url{http://www.research.digital.com/SRC/personal/Leslie_Lamport/home.html}{L. Lamport}
as a base to build the more user-friendly (but less powerful) LaTeX
system.
\end{quote}

Note that the url insertion knows about abbreviating first names (as
for Leslie Lamport) and works over line breaks (as for Donald Knuth).



\section{Usage}
\begin{verbatim}
hlins [options] [inputfile]
\end{verbatim}
Hlins can be used in three different modes (see below).
The following general options exist:
\begin{description}
\item[\texttt{-h}, \texttt{--help}]
Show summary of options and exit.
\item[\texttt{-v}, \texttt{--version}]
Show version of program ad exit.
\item[\texttt{-q}, \texttt{--quiet}]
Surpress diagnostic output.
\item[\texttt{-db}, \texttt{--data-bases} \textit{files} $\ldots$]
Use \textit{files} as address data bases.
The string \textit{files} is a blank-separated list of data base
files, which means that you have to protect the blanks from your shell
when using several data base files. Multiple \verb|-db| options are
accepted.
Examples of usage strings in the
\textit{csh} shell are
\begin{verbatim}
hlins -db myaddresses
hlins -db "friends groupmembers"
hlins -db friends -db groupmembers
\end{verbatim}
The last two invocations are equivalent.
\end{description}

\subsection{Usage in filter mode}
In filter mode, hlins reads html from one source and writes to a
different target. Input is taken from the \textit{inputfile} argument
if existent, otherwise from \textit{stdin}. Output goes by default to
\textit{stdout}.
\begin{description}
\item[\texttt{-o}, \texttt{--output-file} \textit{file}]
Write to \textit{file} instead of standard output.
\end{description}

\subsection{Usage in modify mode}
In modify mode, hlins modifies html files in place.
\begin{description}
\item[\texttt{-m}, \texttt{--modify-files} \textit{files} $\ldots$]
Modify the \textit{files} in-place..
\item[\texttt{-R}, \texttt{--recursive}]
recursively descend into directories and operate on all files with
names ending on \texttt{.html}. Only effective in with the
\texttt{--modify} option.

For instance, ``\texttt{hlins -db $\ldots$ -m ~/WWW -R}'' makes hlins
operate on your complete \texttt{WWW} tree. 
\item[\texttt{-td},\texttt{--tempdir} \textit{dir}]
When doing in-place modifications of files use the directory
\textit{dir} to create temporary files. Default is the value of the
\texttt{TMPDIR} environment variable, and \texttt{/tmp} if
\texttt{TMPDIR} is not set.
\end{description}

\subsection{Usage in database list mode}
\begin{description}
\item[\texttt{--db-to-html}]
Lists the contents of the databases in html to standard output. This
can be handy to create an adress book.
\end{description}

\section{Secondary Effects on the HTML Text}
Hlins replaces special characters of HTML (as \verb|&eacute;| or
\verb|&#233|) by the corresponding ISO-8859-1 character, which is in
this case \verb|�|. Hence, you can use Hlins without any database
argument to replace HTML special characters in a HTML document.

In some cases, non-empty sequences of white space characters may be
replaced by one space. However, this happens only when the white space
is part of a prefix of some name in the data base. Anyway, this
replacement is irrelevant for the rendering of HTML documents.



\section{Address Data Bases}
Every line of the file must be either a comment line or an address
specification. A comment-line is a line that either consists only of
white space, or that starts with the comment-symbol \verb|#| (possibly
preceded by white space).

An address specification consists of a name and a url that are
separated by the character \verb|=| . Leading white space of the line
is ignored. In the name, the character \verb|=| must be written as
\verb|==|. 

Special characters in the name can be either written in HTML or as 8bit
characters. The number of spaces separating the words of a name is not
relevant.

The syntax of the url is not checked.

\section{Variants of Names}
Several variants of the names in the data base are recognized as
well. To find the variants of a name we first split it at white spaces
into \emph{components}.
\begin{itemize}
\item If a name consists of just one component than it has no variant
other than itself.
\item Otherwise, the variants of the name are obtained by considering
all possible combinations of variants of the components. The last
component is treated differently from the other components:
\begin{itemize}
\item If the last component contains the symbol \verb|-| then
the name without this \verb|-| and everything behind is also
recognized. Hence, if you have an entry for \textit{Egon M�ller-Meier}
then \textit{Egon M�ller} is also recognized.
\item A component which is not the last component may be abbreviated,
unless it consists of one only one letter or it terminates on a
dot. The abbreviation of a first name is its first letter followed by
a dot. In case of a word starting with \texttt{St} a further
abbreviation is \texttt{St} followed by a dot, and a word starting on
\texttt{Ch} has additional abbreviation \texttt{Ch} followed by a dot.
Composite first names are abbreviated in both components, hence
\texttt{Marc-Stephane} becomes \texttt{M.-St.} (but not, for instance,
\texttt{M.-Stephane}).
\item In any case generation of variants is surpressed if you write
the component in angular brackets like \texttt{<Marc-Stephane>}.
This mechanism is used in the \url{hlins-doc.adr}{data
base to produce this document}, to have matching of
\verb|Objective Caml| but to avoid matching of \verb|O. Caml|.
\end{itemize}
\end{itemize}


\section{The Exact Rules of Searching Names}
Names are searched starting from the beginning of the text. If there
are overlapping matches then the match starting at the earlier
position wins.  For example, if the data base contains entries for
\verb|Egon Meier| and for \verb|Hans Egon Meier-M�ller| then the second
one matches on input \verb|Hans Egon Meier-M�ller|. 

A match is extended to longer matches if possible. That is, if the
data base contains entries for \verb|Hans Egon| and for
\verb|Hans Egon Meier| then the second one matches on input
\verb|Hans Egon Meier|.


\section{The Exact Rules of URL Insertion}
Hlins does not touch any text between
\verb|<a ... href= ...>| and \verb|</a>|. Note that this applies only if
the \verb|<a>| tag contains the \verb|href| attribute, that is hlins
\emph{does} look at text inside of \verb|<a name=...>| and
\verb|</a>|. As a consequence, hlins is idempotent, that is if you
apply hlins twice (for instance using the \texttt{--modify} option) to
a file you get the same effect than with just one application. Hence, you
can, when you extend your database, safely rerun hlins on your html
files.

The replacment mechanism (including the normalisation of HTML special
charactes) is  shortcut for any text inside the following tags:
\begin{itemize}
\item \verb|<head>| $\ldots$ \verb|</head>|
\item \verb|<samp>| $\ldots$ \verb|</samp>|
\item \verb|<kbd>| $\ldots$ \verb|</kbd>|
\item \verb|<pre>| $\ldots$ \verb|</pre>|
\item \verb|<div nohlins>| $\ldots$ \verb|</div>|
\end{itemize}

The rationale is that the first four tags of this list are intended to
mark some kind of verbatim text (see the
\url{htttp:/www.w3.org/TR/html401/}{HTML 4.01 specification}). The
last one is an escape mechanism in case you have to overrule hlins'
mechanism.  Text from the beginning of one of the start
tags to the first occurrence of the corresping end mark is ignored.
The consequence is that among the above list embedded tags of the same
kind are not correctly treated.

Furthermore, text inside angular brackets \verb|<| and \verb|>| is not
treated by hlins. 

If there are several different url's for a string \textit{foundname}
then the following rules apply to determine the url inserted:
\begin{enumerate}
\item An address specification ``\textit{name} = \textit{url}'' where
\textit{name} matches exactly (modulo white space and HTML special
characters) \textit{foundname} has priority over a name specification
``\textit{name} = \textit{url}'' where \textit{foundname} is an
abbreviation for \textit{name}.
\item In the list obtained from the above priority rule, the first
match is taken.
\end{enumerate}

A warning is issued in case of a conflict, unless the \verb|--quiet|
option has been given.

For instance, your data base might contain something like
\begin{quote}
\begin{alltt}
Hans Meyer   =  http://address.for.full.name
H. Meyer     =  http://address.for.abbreviated.name
\end{alltt}
\end{quote}
On input \verb|H. Meyer|, the second address specification is selected
(and a warning is issued).

\section{Implementation}
Hlins is written in \ahref{http://caml.inria.fr/ocaml}{Objective Caml}.

\section{License and Installation}
Hins ins covered by the
\ahref{http://www.fsf.org/licenses/gpl.html}{Gnu General Public
  License}.  See the \ahref{https://hlins.alioth.debian.org}{Hlins
  home page} for binary and source distributions.

\section{Credits}
Thanks to Claude March� and Jean-Christophe Filli\^atre for their
remarks and suggestions.

\end{document}




