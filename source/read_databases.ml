(***************************************************************************)
(*  HLins: insert http-links into HTML documents.                          *)
(*  See http://www.lri.fr/~treinen/hlins                                   *)
(*                                                                         *)
(*  Copyright (C) 1999-2024 Ralf Treinen <treinen@irif.fr>                 *)
(*                                                                         *)
(*  This program is free software; you can redistribute it and/or modify   *)
(*  it under the terms of the GNU General Public License as published by   *)
(*  the Free Software Foundation; either version 2 of the License, or (at  *)
(*  your option) any later version.                                        *)
(*                                                                         *)
(*  This program is distributed in the hope that it will be useful, but    *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU      *)
(*  General Public License for more details.                               *)
(*                                                                         *)
(*  You should have received a copy of the GNU General Public License      *)
(*  along with this program; if not, write to the Free Software            *)
(*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307    *)
(*  USA                                                                    *)
(*                                                                         *)
(***************************************************************************)

(*
    HLins: insert http-links into HTML documents.
    See http://www.lri.fr/~treinen/hlins

    Copyright (C) 1999 Ralf Treinen <treinen@lri.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)


open Lexing;;
open Addr_lex;;
open Html_chars;;
open Errors;;


(* (read_dblexbuf filename lb names urls) reads from the lexbuf lb the
   rest of a database file named filename, and returns the pair of
   lists (names',urls'), where names' (urls') is the list of names
   (urls) read in reverse order, concatenated with names (urls).
*)

let rec read_dblexbuf filename lb names urls =
  try
      let (name,url) = next_name_sep_url lb
      in read_dblexbuf filename lb (name::names) (url::urls)
  with
      Eof -> (names,urls)
    | Error_lex (n,s) ->
	stopwitherror
	  ("Data base "^filename^", line "^(string_of_int n) ^": "^s)
;;


let read_dbs filelist =
  List.fold_left
    (fun (names,urls) filename ->
       try
	 let c = open_in filename in
	 let lb = from_channel c
	 in let result = 
	     read_dblexbuf filename
	       (from_function (fun s _ ->
			try
				Bytes.set s 0 (next_char lb); 1
			with
				End_of_file -> 0))
	       names urls
	 in
	   begin
	     close_in c;
	     result
	   end
       with
	   Sys_error s -> stopwitherror ("Cannot read data base file "^s))
    ([],[])
    filelist
;;





