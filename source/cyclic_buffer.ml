(***************************************************************************)
(*  HLins: insert http-links into HTML documents.                          *)
(*  See http://www.lri.fr/~treinen/hlins                                   *)
(*                                                                         *)
(*  Copyright (C) 1999-2024 Ralf Treinen <treinen@irif.fr>                 *)
(*                                                                         *)
(*  This program is free software; you can redistribute it and/or modify   *)
(*  it under the terms of the GNU General Public License as published by   *)
(*  the Free Software Foundation; either version 2 of the License, or (at  *)
(*  your option) any later version.                                        *)
(*                                                                         *)
(*  This program is distributed in the hope that it will be useful, but    *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU      *)
(*  General Public License for more details.                               *)
(*                                                                         *)
(*  You should have received a copy of the GNU General Public License      *)
(*  along with this program; if not, write to the Free Software            *)
(*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307    *)
(*  USA                                                                    *)
(*                                                                         *)
(***************************************************************************)

(*
    HLins: insert http-links into HTML documents.
    See http://www.lri.fr/~treinen/hlins

    Copyright (C) 1999 Ralf Treinen <treinen@lri.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

open Bytes;;

type buffer = 
    { buf: bytes;         (* stores the contents *)
      len: int;            (* length of buf *)
      mutable sta: int;    (* position of the first char *)
      mutable uno: int     (* first unoccupied position *)
    }
;;

let fresh n =
    { buf = create n;
      len = n;
      sta = 0;
      uno = 0
    };;

let is_empty {sta=s;uno=e} = ( s = e );;

let addc b c =
  Bytes.set b.buf b.uno c;
  b.uno <- (b.uno + 1) mod b.len
;;

let getc b =
  let res = Bytes.get b.buf b.sta
  in
    b.sta <- (b.sta + 1) mod b.len;
    res
;;

let gets b n =
  let oldsta = b.sta
  and oldstan= b.sta+n
  in
    if oldstan < b.len
    then
      begin
	b.sta <- oldstan;
	sub_string b.buf oldsta n
      end
    else
      begin
	b.sta <- oldstan mod b.len;
	(sub_string b.buf oldsta (b.len - oldsta)) ^
	(sub_string b.buf 0 b.sta)
      end
;;

let getall b =
  let olduno = b.uno
  in
    begin
      b.uno <- b.sta;
      if olduno >= b.sta
      then (Bytes.sub_string b.buf b.sta (olduno-b.sta))
      else (Bytes.sub_string b.buf b.sta (b.len-b.sta))^
	(Bytes.sub_string b.buf 0 olduno);
    end
;;

let push b s =
  let l = String.length s
  in
    if l <= b.sta
    then
      begin
	String.blit s 0 b.buf (b.sta-l) l;
	b.sta <- b.sta - l
      end
    else
      begin
	String.blit s (l-b.sta) b.buf 0 b.sta;
	String.blit s 0 b.buf (b.len-l+b.sta) (l-b.sta);
	b.sta <- b.len-l+b.sta
      end
;;






