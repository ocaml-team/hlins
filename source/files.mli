(***************************************************************************)
(*  HLins: insert http-links into HTML documents.                          *)
(*  See http://www.lri.fr/~treinen/hlins                                   *)
(*                                                                         *)
(*  Copyright (C) 1999-2024 Ralf Treinen <treinen@irif.fr>                 *)
(*                                                                         *)
(*  This program is free software; you can redistribute it and/or modify   *)
(*  it under the terms of the GNU General Public License as published by   *)
(*  the Free Software Foundation; either version 2 of the License, or (at  *)
(*  your option) any later version.                                        *)
(*                                                                         *)
(*  This program is distributed in the hope that it will be useful, but    *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU      *)
(*  General Public License for more details.                               *)
(*                                                                         *)
(*  You should have received a copy of the GNU General Public License      *)
(*  along with this program; if not, write to the Free Software            *)
(*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307    *)
(*  USA                                                                    *)
(*                                                                         *)
(***************************************************************************)

(*
    HLins: insert http-links into HTML documents.
    See http://www.lri.fr/~treinen/hlins

    Copyright (C) 1999 Ralf Treinen <treinen@lri.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*************************************************************************)
(* Functions on files. This is Unix-specific.                            *)
(*************************************************************************)

(* test wether two filenames designate physically the same file (that
   is, wether they are located on the same devices and have same inode). 
   the test result is false if either of the files does not exist or
   if its status is not accessible. An empty filename is considered
   different from any other filename.
*)

val samefile : string -> string -> bool;;

(* (tmpfile dir) returns the name of a temporary file in dir *)

exception Error_tmpfile of string;;
val newtmpfile: string -> string;;

(* (move s1 s2) moves file s1 to s2, by changing links when both are
   on the same filesystem, and copying else
*)
exception Error_move of string;;
val move: string -> string -> unit;;

(* (scandir path dir) yields the pair (hl,dl) where
   - hl is the list of regular files *.html in path/dir
   - dl is the list of directories in in path/dir.
   Stops with error when the directory path/dir cannot be read.
*)
val scandir: string -> string -> string list * string list;;

(* (select p l), where l is a list of filenames and p a path, returns
   the pair (hl,dl) where hl is the list of html files and sl is the
   list of directories in l (except . and ..). Stops with error when
   the status of one of the files in l is not accessible.
*)
val select : string -> string list -> string list * string list;;

(* (expanddot fl), where l is a list of files and directories,
   replaces the first occurrence of . by a listing of the current
   directory.
*)
val expanddot : string list -> string list;;
