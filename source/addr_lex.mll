(***************************************************************************)
(*  HLins: insert http-links into HTML documents.                          *)
(*  See http://www.lri.fr/~treinen/hlins                                   *)
(*                                                                         *)
(*  Copyright (C) 1999-2024 Ralf Treinen <treinen@irif.fr>                 *)
(*                                                                         *)
(*  This program is free software; you can redistribute it and/or modify   *)
(*  it under the terms of the GNU General Public License as published by   *)
(*  the Free Software Foundation; either version 2 of the License, or (at  *)
(*  your option) any later version.                                        *)
(*                                                                         *)
(*  This program is distributed in the hope that it will be useful, but    *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU      *)
(*  General Public License for more details.                               *)
(*                                                                         *)
(*  You should have received a copy of the GNU General Public License      *)
(*  along with this program; if not, write to the Free Software            *)
(*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307    *)
(*  USA                                                                    *)
(*                                                                         *)
(***************************************************************************)

(*
    HLins: insert http-links into HTML documents.
    See http://www.lri.fr/~treinen/hlins

    Copyright (C) 1999 Ralf Treinen <treinen@lri.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)


{
  let ln = ref 1;; (* line number *)
  exception Error_lex of int*string;;
  exception Eof;;
}


let linespace = [' ''\t']


(* 
    next_name_tab_url: called when we expect a name. Ignore any line
    that starts with the comment sign, and any white space. We accept
    as a name any string that does not contain "=" or newline, and that
    does not end or start on white space. The character "=" can be
    escaped as "==".
*)

rule next_name_sep_url = parse
    '#'[^'\n']*('\n')	       { incr ln; next_name_sep_url lexbuf }
  | linespace+  	       { next_name_sep_url lexbuf }
  | '\n'                       { incr ln; next_name_sep_url lexbuf }
  | [^' ''#''\t''\n''='](([^'=''\n']|"==")*[^' ''\t''\n''='])?
                               { let name = Lexing.lexeme lexbuf
                          			in (name , sep_url lexbuf) }
  | eof					{ raise Eof }


(* separator *)
and sep_url = parse
    linespace* '=' linespace* { url lexbuf }
  | _     		{ raise (Error_lex (!ln,"No separator found"))}


(*
    any non-empty string up to the end of the line is accepted as url
*)
and url = parse
    [^'\n']+	{ Lexing.lexeme lexbuf }
  | ('\n'|eof)	{ raise (Error_lex (!ln,"No URL found"))}


