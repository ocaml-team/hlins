(***************************************************************************)
(*  HLins: insert http-links into HTML documents.                          *)
(*  See http://www.lri.fr/~treinen/hlins                                   *)
(*                                                                         *)
(*  Copyright (C) 1999-2024 Ralf Treinen <treinen@irif.fr>                 *)
(*                                                                         *)
(*  This program is free software; you can redistribute it and/or modify   *)
(*  it under the terms of the GNU General Public License as published by   *)
(*  the Free Software Foundation; either version 2 of the License, or (at  *)
(*  your option) any later version.                                        *)
(*                                                                         *)
(*  This program is distributed in the hope that it will be useful, but    *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU      *)
(*  General Public License for more details.                               *)
(*                                                                         *)
(*  You should have received a copy of the GNU General Public License      *)
(*  along with this program; if not, write to the Free Software            *)
(*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307    *)
(*  USA                                                                    *)
(*                                                                         *)
(***************************************************************************)

(*
    HLins: insert http-links into HTML documents.
    See http://www.lri.fr/~treinen/hlins

    Copyright (C) 1999 Ralf Treinen <treinen@lri.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

(*

(* implementation of transitions tables by lists *)
  type transitions = (char*int) list;;
  let empty_transitions = [];;
  let get_transition t c = List.assoc c t;;
  let add_transition t c p = (c,p)::t;;
  let transitions_fold f = List.fold_left (fun x (c,q) -> (f x c q));;
*)


(* implementation of transition tables by finite functions *)
module OrderedChar =
  struct
    type t = char
    (* let compare x y = (Char.code x) - (Char.code y) *)
    let compare = Stdlib.compare
  end;;

module M = Map.Make(OrderedChar);;
  type transitions = int M.t;;
  let empty_transitions = M.empty;;
  let get_transition t c = M.find c t;;
  let add_transition t c q = M.add c q t;;
  let transitions_fold f i t = M.fold (fun c i x -> f x c i) t i;;



type automaton = {
  max_path_length:   int;
  number_of_states:  int;
  tree:              transitions array;
  level:             int array;               
  board:             int array;
  suf:               int array;
  found:             (int list) array;
  expand:            (int list) array
};;                
