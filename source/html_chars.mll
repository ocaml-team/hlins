(***************************************************************************)
(*  HLins: insert http-links into HTML documents.                          *)
(*  See http://www.lri.fr/~treinen/hlins                                   *)
(*                                                                         *)
(*  Copyright (C) 1999-2024 Ralf Treinen <treinen@irif.fr>                 *)
(*                                                                         *)
(*  This program is free software; you can redistribute it and/or modify   *)
(*  it under the terms of the GNU General Public License as published by   *)
(*  the Free Software Foundation; either version 2 of the License, or (at  *)
(*  your option) any later version.                                        *)
(*                                                                         *)
(*  This program is distributed in the hope that it will be useful, but    *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU      *)
(*  General Public License for more details.                               *)
(*                                                                         *)
(*  You should have received a copy of the GNU General Public License      *)
(*  along with this program; if not, write to the Free Software            *)
(*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307    *)
(*  USA                                                                    *)
(*                                                                         *)
(***************************************************************************)

(*
    HLins: insert http-links into HTML documents.
    See http://www.lri.fr/~treinen/hlins

    Copyright (C) 1999 Ralf Treinen <treinen@lri.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

{
open Lexing;;

(* translates an html character representation "&#nnn;" to the 
   corresponding 8-bit char.
*)
let special_to_char s = Char.chr (int_of_string (String.sub s 2 3))
;;

}


rule next_char = parse
  | "&Agrave;"	{ '�' }
  | "&Aacute;"	{ '�' }
  | "&Acirc;"	{ '�' }
  | "&Atilde;"	{ '�' }
  | "&Auml;"	{ '�' }
  | "&Aring;"	{ '�' }
  | "&AElig;"	{ '�' }
  | "&Egrave;"	{ '�' }
  | "&Eacute;"	{ '�' }
  | "&Ecirc;"	{ '�' }
  | "&Euml;"	{ '�' }
  | "&Igrave;"	{ '�' }
  | "&Iacute;"	{ '�' }
  | "&Icirc;"	{ '�' }
  | "&Iuml;"	{ '�' }
  | "&ETH;"	{ '�' }
  | "&Ntilde;"	{ '�' }
  | "&Ograve;"	{ '�' }
  | "&Oacute;"	{ '�' }
  | "&Ocirc;"	{ '�' }
  | "&Otilde;"	{ '�' }
  | "&Ouml;"	{ '�' }
  | "&Oslash;"	{ '�' }
  | "&Ugrave;"	{ '�' }
  | "&Uacute;"	{ '�' }
  | "&Ucirc;"	{ '�' }
  | "&Uuml;"	{ '�' }
  | "&Yacute;"	{ '�' }
  | "&THORN;"	{ '�' }
  | "&szlig;"	{ '�' }
  | "&agrave;"	{ '�' }
  | "&aacute;"	{ '�' }
  | "&acirc;"	{ '�' }
  | "&atilde;"	{ '�' }
  | "&auml;"	{ '�' }
  | "&aring;"	{ '�' }
  | "&aelig;"	{ '�' }
  | "&ccdil;"	{ '�' }
  | "&egrave;"	{ '�' }
  | "&eacute;"	{ '�' }
  | "&ecirc;"	{ '�' }
  | "&euml;"	{ '�' }
  | "&igrave;"	{ '�' }
  | "&iacute;"	{ '�' }
  | "&icirc;"	{ '�' }
  | "&iuml;"	{ '�' }
  | "&eth;"	{ '�' }
  | "&ntilde;"	{ '�' }
  | "&ograve;"	{ '�' }
  | "&oacute;"	{ '�' }
  | "&ocirc;"	{ '�' }
  | "&otilde;"	{ '�' }
  | "&ugrave;"	{ '�' }
  | "&uacute;"	{ '�' }
  | "&ucirc;"	{ '�' }
  | "&uuml;"	{ '�' }
  | "&yacute;"	{ '�' }
  | "&thorn;"	{ '�' }
      (* special chaarcters between 192 and 255 *)
  | "&#" ( "19"['2'-'9'] | '2'['0'-'4']['0'-'9'] | "25"['0'-'5'] ) ";"
      { special_to_char (lexeme lexbuf) }
  |  eof		{ raise End_of_file }
  | _             { lexeme_char lexbuf 0}


